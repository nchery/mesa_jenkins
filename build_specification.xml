<!--  Copyright (C) Intel Corp.  2014.  All Rights Reserved. -->

<!--  Permission is hereby granted, free of charge, to any person obtaining -->
<!--  a copy of this software and associated documentation files (the -->
<!--  "Software"), to deal in the Software without restriction, including -->
<!--  without limitation the rights to use, copy, modify, merge, publish, -->
<!--  distribute, sublicense, and/or sell copies of the Software, and to -->
<!--  permit persons to whom the Software is furnished to do so, subject to -->
<!--  the following conditions: -->

<!--  The above copyright notice and this permission notice (including the -->
<!--  next paragraph) shall be included in all copies or substantial -->
<!--  portions of the Software. -->

<!--  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, -->
<!--  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF -->
<!--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. -->
<!--  IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE -->
<!--  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION -->
<!--  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION -->
<!--  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->

<!--   **********************************************************************/ -->
<!--   * Authors: -->
<!--   *   Mark Janes <mark.a.janes@intel.com> -->
<!--   **********************************************************************/ -->

<build_specification>
  <build_master host="otc-mesa-ci.jf.intel.com" results_dir="/mnt/jenkins/results"
                hostname="otc-mesa-ci"/>

  <!-- specified the dependency relationships between projects -->
  <projects>
    
    <!-- each project has a matching subdirectory with a build.py
         which automates the build.  -->

    <project name="android-buildtest" src_dir="mesa"/>

    <project name="sim-drm"/>

    <project name="fulsim">
      <prerequisite name="sim-drm" hardware="builder" arch="m64"/>
    </project>

    <project name="drm"/>

    <project name="mesa">
      <!-- options for a prerequisite are inherited unless overridden.
           For example, a build of mesa with arch=m32 will require a
           build of drm with arch=m32 -->
      <prerequisite name="drm"/>
    </project>

    <!-- only works on m64, due to llvm dependencies -->
    <project name="mesa-buildtest" src_dir="mesa">
      <prerequisite name="drm"/>
    </project>

    <project name="waffle"/>

    <!-- test projects should specify bisect_hardware and bisect_arch,
         which designate the full set of platforms that should be
         re-tested when a regression occurs.  See update_conf.py and
         bisect_project.py in scripts. -->
    <project name="deqp-test" src_dir="deqp"
             bisect_hardware="bdw_iris,gen9_iris,icl_iris,tgl"
             bisect_arch="m64,m32">
      <!-- deqp-test will be specificed for a test platform (eg skl),
           but the hardware for its prerequisites should be a builder
           (since they are all compilation projects) -->
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp-runtime">
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp">
      <prerequisite name="mesa"/>
    </project>

    <project name="glescts" src_dir="cts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts-test"
             bisect_hardware="gen9_iris,icl_iris,bdw_iris,tgl"
             bisect_arch="m64,m32">
      <prerequisite name="glcts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-test"
             bisect_hardware="bdw_iris,gen9_iris,icl_iris,gen9atom_iris,tgl,dg1"
             bisect_arch="m64">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="glescts" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="mi-builder-test">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-full">
      <prerequisite name="glescts-test" hardware="bdw_iris,gen9_iris,icl_iris,tgl"/>
      <!-- only_for_type enforces the prerequisite when type matches
           the value. -->
      <prerequisite name="glescts-test" hardware="gen9atom_iris" only_for_type="daily"/>
      <prerequisite name="glescts-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="bdw_iris,gen9_iris,icl_iris"/>
      <prerequisite name="glescts-test" only_for_type="daily" hardware="dg1" arch="m64"/>
    </project>

    <project name="vkrunner"/>
      
    <project name="piglit">
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="vkrunner" hardware="builder"/>
    </project>

    <project name="piglit-test"
             bisect_hardware="gen9_iris,gen9atom_iris,bdw_iris,icl_iris,tgl"
             bisect_arch="m64,m32">
      <prerequisite name="piglit" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim,dg1_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim,dg1_sim"/>
    </project>

    <project name="piglit-full">
      <prerequisite name="piglit-test"
                    hardware="bdw_iris,gen9_iris,icl_iris,tgl"
                    shard="2"/>
      <prerequisite name="piglit-test"
                    only_for_type="daily"
                    hardware="gen9_iris,icl_iris"
                    arch="m32"
                    shard="2"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="gen9atom_iris"
                    arch="m64,m32" shard="4"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="dg1"
                    arch="m64"
                    shard="2"/>
    </project>
    
    <project name="crucible">
      <prerequisite name="mesa"/>
    </project>

    <project name="crucible-test"
             bisect_hardware="bdw,ivb,bsw,byt,hsw,gen9atom,gen9,icl,tgl,dg1"
             bisect_arch="m64">
      <prerequisite name="crucible" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim,dg1_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim,dg1_sim"/>
    </project>

    <project name="crucible-full">
      <prerequisite name="crucible-test" hardware="bdw,bsw,hsw,gen9atom,gen9,icl,tgl"/>
      <prerequisite name="crucible-test" only_for_type="daily" arch="m64" hardware="gen9atom,ivb,byt"/>
      <prerequisite name="crucible-test" only_for_type="daily" hardware="dg1" arch="m64"/>
    </project>

    <!-- <project name="skqp" /> -->

    <!-- <project name="skqp-test" -->
    <!--          bisect_hardware="gen9atom,icl,icl_iris,gen9,gen9_iris,gen9atom_iris" -->
    <!--          bisect_arch="m64"> -->
    <!--   <prerequisite name="waffle" hardware="builder"/> -->
    <!--   <prerequisite name="mesa" hardware="builder"/> -->
    <!--   <prerequisite name="skqp" hardware="builder"/> -->
    <!-- </project> -->

    <!-- <project name="skqp-full"> -->
    <!--   <prerequisite name="skqp-test" hardware="gen9,gen9_iris,icl_iris,bdw,bdw_iris,tgl" arch="m64"/> -->
    <!--   <prerequisite only_for_type="daily" name="skqp-test" hardware="gen9atom,gen9atom_iris,icl" arch="m64"/> -->
    <!-- </project> -->

    <project name="vulkancts-test" src_dir="vulkancts"
             bisect_hardware="bdw,bsw,hsw,gen9atom,gen9,icl,tgl,dg1"
             bisect_arch="m64">
      <prerequisite name="vulkancts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" only_for_hardware="tgl_sim,dg1_sim"/>
      <prerequisite name="fulsim" only_for_hardware="tgl_sim,dg1_sim"/>
    </project>

    <project name="vulkancts-full">
      <prerequisite name="vulkancts-test" hardware="gen9,tgl" shard="4"/>
      <prerequisite name="vulkancts-test" hardware="icl" shard="6"/>
      <prerequisite name="vulkancts-test" hardware="hsw,bdw" shard="6"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bdw,hsw,gen9,icl"
                    arch="m32"
                    shard="4"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bsw,gen9atom"
                    arch="m64"
                    shard="10"/>
      <prerequisite name="vulkancts-test" only_for_type="daily" hardware="dg1"
		    shard="6" arch="m64"/>

      <prerequisite name="crucible-full"/>
    </project>

    <project name="mi-builder-full">
      <prerequisite name="mi-builder-test" hardware="icl,tgl"/>
    </project>

    <project name="vulkancts"/>

    <project name="all-test-vulkan">
      <prerequisite name="all-test"/>
      <prerequisite name="vulkancts-full"/>
    </project>

    <project name="test-vulkan">
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="android-buildtest" hardware="builder"/>
      <prerequisite name="vulkancts-full"/>
      <prerequisite name="crucible-full"/>
    </project>

    <!-- all-test has no build.py, and exists only to provide
         dependency relationships. -->
    <project name="all-test">
      <prerequisite name="piglit-full"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="android-buildtest" hardware="builder"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <!-- <prerequisite name="skqp-full"/> -->
      <!-- <prerequisite name="webgl-full"/> -->
      <prerequisite name="mi-builder-full"/>
    </project>

    <project name="all-test-iris">
      <prerequisite name="piglit-test"
                    hardware="gen9_iris,bdw_iris"
                    shard="2"/>
      <prerequisite name="piglit-test"
                    hardware="icl_iris" shard="4"/>
      <prerequisite name="glcts-test"
                    hardware="gen9_iris,icl_iris,bdw_iris"/>
      <!-- <prerequisite name="skqp-test" hardware="gen9_iris,icl_iris" arch="m64"/> -->
    </project>

    <project name="test-single-arch-simdrm">
      <prerequisite name="piglit-test" hardware="tgl_sim,dg1_sim" arch="m64" shard="3"/>
      <prerequisite name="crucible-test" hardware="tgl_sim,dg1_sim" arch="m64"/>
      <prerequisite name="vulkancts-test" hardware="tgl_sim,dg1_sim" arch="m64" shard="4"/>
    </project>

    <!-- For testing internal mesa branch -->
    <project name="test-single-arch-internal">
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="test-single-arch-gen9"/>
      <prerequisite name="test-single-arch-tgl"/>
      <prerequisite name="test-single-arch-dg1"/>
    </project>

    <project name="test-single-arch-tgl-gl">
      <prerequisite name="piglit-test" hardware="tgl" shard="2" arch="m64"/>
      <prerequisite name="deqp-test" hardware="tgl" shard="3" arch="m64"/>
      <prerequisite name="glcts-test" hardware="tgl" arch="m64"/>
      <prerequisite name="glescts-test" hardware="tgl" arch="m64"/>
      <prerequisite name="mi-builder-test" hardware="tgl"/>
      <!-- <prerequisite name="skqp-test" hardware="tgl"/> -->
    </project>

    <project name="test-single-arch-tgl-vulkan">
      <prerequisite name="vulkancts-test" hardware="tgl" shard="8" arch="m64"/>
      <prerequisite name="crucible-test" hardware="tgl" arch="m64"/>
    </project>

    <project name="test-single-arch-tgl">
      <prerequisite name="test-single-arch-tgl-vulkan"/>
      <prerequisite name="test-single-arch-tgl-gl"/>
    </project>

    <project name="test-single-arch-tgl-sim">
        <prerequisite name="piglit-test" hardware="tgl_sim" arch="m64" shard="3"/>
        <prerequisite name="crucible-test" hardware="tgl_sim" arch="m64"/>
    </project>

    <project name="test-single-arch-gen9-gl">
      <prerequisite name="piglit-test" hardware="gen9_iris" shard="2" arch="m64"/>
      <prerequisite name="deqp-test" hardware="gen9_iris" shard="4" arch="m64"/>
      <prerequisite name="glcts-test" hardware="gen9_iris" arch="m64"/>
      <prerequisite name="glescts-test" hardware="gen9_iris" arch="m64"/>
      <!-- <prerequisite name="skqp-test" hardware="gen9_iris"/> -->
    </project>

    <project name="test-single-arch-gen9-vulkan">
      <prerequisite name="vulkancts-test" hardware="gen9" shard="8" arch="m64"/>
      <prerequisite name="crucible-test" hardware="gen9" arch="m64"/>
    </project>

    <project name="test-single-arch-gen9">
      <prerequisite name="test-single-arch-gen9-vulkan"/>
      <prerequisite name="test-single-arch-gen9-gl"/>
    </project>

    <project name="test-single-arch-vulkan">
      <prerequisite name="test-single-arch"/>
      <prerequisite name="vulkancts-full"/>
    </project>
    <project name="test-single-arch-dg1-sim">
      <prerequisite name="piglit-test" hardware="dg1_sim" arch="m64" shard="6"/>
      <prerequisite name="crucible-test" hardware="dg1_sim" arch="m64"/>
      <prerequisite name="vulkancts-test" hardware="dg1_sim" arch="m64" shard="4"/>
   </project>

    <project name="test-single-arch-dg1">
      <prerequisite name="piglit-test" hardware="dg1" shard="2" arch="m64"/>
      <prerequisite name="glcts-test" hardware="dg1" arch="m64"/>
      <prerequisite name="crucible-test" hardware="dg1" arch="m64"/>
      <prerequisite name="glescts-test" hardware="dg1" arch="m64"/>
      <prerequisite name="deqp-test" hardware="dg1" shard="2" arch="m64"/>
      <prerequisite name="vulkancts-test" hardware="dg1" shard="6" arch="m64"/>
      <prerequisite name="mi-builder-test" hardware="dg1"/>
    </project>

    <project name="test-single-arch">
      <prerequisite name="piglit-full"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="gen9atom_iris,icl" shard="4"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="android-buildtest" hardware="builder"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <!-- <prerequisite name="skqp-full"/> -->
      <prerequisite name="mi-builder-full"/>
      <!-- <prerequisite name="webgl-full"/> -->
    </project>

    <project name="test-single-arch-crocus">
      <prerequisite name="piglit-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus" shard="2"/>
      <prerequisite name="piglit-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus" shard="4"/>
      <prerequisite name="deqp-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus" shard="4"/>
      <prerequisite name="glcts-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus"/>
      <prerequisite name="glcts-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus"/>
      <prerequisite name="glescts-test" hardware="snb_crocus,ivb_crocus,bsw_crocus,hsw_crocus"/>
      <prerequisite name="glescts-test"
                    only_for_type="daily"
                    hardware="ilk_crocus,byt_crocus"/>
    </project>

    <project name="deqp-full">
      <prerequisite name="deqp-test" hardware="bdw_iris,gen9_iris,icl_iris,tgl" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="bdw_iris,gen9_iris" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    hardware="icl_iris,tgl" shard="3" arch="m64,m32"/>
      <prerequisite name="deqp-test" only_for_type="daily" hardware="dg1" shard="2" arch="m64"/>
    </project>

    <project name="deqp-runtime-full">
      <prerequisite name="deqp-runtime" hardware="bdw_iris,gen9_iris" shard="5"/>
      <prerequisite name="deqp-runtime" hardware="gen9atom_iris" shard="8"/>
    </project>

    <project name="glcts-full">
      <prerequisite name="glcts-test" hardware="bdw_iris,gen9_iris,tgl" arch="m64"/>
      <prerequisite name="glcts-test" hardware="icl_iris" shard="4"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="bdw_iris" arch="m32"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="icl" shard="4"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="icl,icl_iris" arch="m32" shard="4"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="dg1" arch="m64"/>
    </project>

    <project name="webgl" bisect_hardware="gen9_iris,icl_iris,tgl">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="webgl-full">
      <prerequisite name="webgl" hardware="gen9_iris,icl_iris,tgl" shard="6"/>
    </project>
    
    <project name="reboot-slave"/>

    <project name="piglit-gen12">
      <prerequisite name="piglit-test" hardware="tgl" shard="2" arch="m64"/>
    </project>

  </projects>

  <!-- the following servers and remotes correspond to projects -->
  <repos>
    <mesa repo="https://gitlab.freedesktop.org/mesa/mesa.git"
            branch="origin/main">
      <remote name="ci_mesa_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/mesa.git"/>
      <remote name="ci_internal_rebase_mesa_repo" repo="ssh://git@gitlab.devtools.intel.com:29418/mesa_ci/repos/mesa-rebase-testing.git"/>
      <remote name="jekstrand" repo="https://gitlab.freedesktop.org/jekstrand/mesa.git"/>
      <remote name="tpalli" repo="https://gitlab.freedesktop.org/tpalli/mesa.git"/>
      <remote name="dcbaker" repo="https://gitlab.freedesktop.org/dbaker/mesa.git"/>
      <remote name="tarceri" repo="https://gitlab.freedesktop.org/tarceri/mesa.git"/>
      <remote name="evelikov" repo="git://github.com/evelikov/mesa"/>
      <remote name="imirkin" repo="git://github.com/imirkin/mesa"/>
      <remote name="igalia_release" repo="git://github.com/Igalia/release-mesa"/>
      <remote name="internal-gitlab" repo="ssh://git@gitlab.devtools.intel.com:29418/linux-gfx/mesa/mesa.git"/>
      <remote name="internal-github" repo="git@github.com:intel-innersource/drivers.gpu.mesa.dev.git"/>
      <remote name="daniels" repo="https://gitlab.freedesktop.org/daniels/mesa.git"/>
      <remote name="anholt" repo="https://gitlab.freedesktop.org/anholt/mesa.git"/>
      <remote name="eric_engestrom" repo="https://gitlab.freedesktop.org/eric/mesa"/>
      <remote name="airlied" repo="https://gitlab.freedesktop.org/airlied/mesa.git"/>
      <remote name="global_logic" repo="https://gitlab.freedesktop.org/GL/mesa.git"/>
      <remote name="karolherbst" repo="https://github.com/karolherbst/mesa.git"/>
      <remote name="cwabbott" repo="https://gitlab.freedesktop.org/cwabbott0/mesa.git"/>
      <remote name="kusma" repo="https://gitlab.freedesktop.org/kusma/mesa"/>
      <remote name= "gerddie" repo="https://gitlab.freedesktop.org/gerddie/mesa"/>
    </mesa>

    <drm repo="http://anongit.freedesktop.org/git/mesa/drm.git"
         branch="origin/main">
      <remote name="aphogat" repo="git://github.com/aphogat/drm"/>
    </drm>

    <vkrunner repo="git://github.com/Igalia/vkrunner.git" branch="origin/master"/>

    <piglit repo="https://gitlab.freedesktop.org/mesa/piglit.git"
            branch="origin/main">
      <remote name="ci_piglit_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/piglit.git"/>
      <remote name="jekstrand" repo="https://gitlab.freedesktop.org/jekstrand/piglit.git"/>
      <!-- Dylan's repository is more stable than the origin, and will
           not trigger builds unnecessarily -->
      <remote name="dcbaker" repo="https://gitlab.freedesktop.org/dbaker/piglit.git"/>
      <remote name="nchery-gitlab" repo="https://gitlab.freedesktop.org/nchery/piglit.git"/>
      <remote name="antognolli" repo="https://gitlab.freedesktop.org/rantogno/piglit.git"/>
      <remote name="eric_engestrom" repo="https://gitlab.freedesktop.org/eric/piglit"/>
    </piglit>

    <waffle repo="https://gitlab.freedesktop.org/mesa/waffle.git">
      <remote name="jekstrand" repo="git://github.com/jekstrand/waffle"/>
    </waffle>

    <!-- just so the master can cache it for the builders -->
    <mesa_jenkins repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_jenkins.git">
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_jenkins.git"/>
    </mesa_jenkins>

    <mesa_ci repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_ci.git">
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_ci.git"/>
      <remote name="ybogdano" repo="https://gitlab.freedesktop.org/ybogdano/meson-flag-patch.git"/>
    </mesa_ci>

    <deqp repo="https://android.googlesource.com/platform/external/deqp"
          branch="remotes/origin/upstream-vulkan-cts-1.2.7">
    </deqp>

    <cts repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"
         branch="origin/opengl-es-cts-3.2.8">
    </cts>

    <glcts repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"
           branch="origin/opengl-cts-4.6.2">
    </glcts>

    <gtest repo="https://android.googlesource.com/platform/external/googletest"/>
    <glslang repo="git://github.com/KhronosGroup/glslang.git">
      <remote name="glsl" repo="git@gitlab.khronos.org:GLSL/glslang.git"/>
    </glslang>

    <crucible repo="https://gitlab.freedesktop.org/mesa/crucible.git"
              branch="origin/main">
      <remote name="ci_crucible_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/crucible.git"/>
    </crucible>

    <vulkancts repo="https://github.com/KhronosGroup/VK-GL-CTS.git"
               branch="tags/vulkan-cts-1.3.1.0">
      <remote name="khronos" repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"/>
    </vulkancts>
    
    <spirvtools repo="git://github.com/KhronosGroup/SPIRV-Tools.git"
                branch="khronos/master">
      <remote name="khronos" repo="ssh://git@gitlab.khronos.org/spirv/spirv-tools.git"/>
    </spirvtools>
    
    <spirvheaders repo="git://github.com/KhronosGroup/SPIRV-Headers"
                  branch="origin/master">
      <remote name="khronos" repo="git@gitlab.khronos.org:spirv/SPIRV-Headers.git"/>
    </spirvheaders>

    <vulkandocs repo="https://github.com/KhronosGroup/Vulkan-Docs.git"
                branch="origin/main">
    </vulkandocs>

    <kc-cts repo="git@gitlab.khronos.org:opengl/kc-cts.git"/>

    <mesa_ci_internal repo="ssh://git@gitlab.devtools.intel.com:29418/mesa_ci/mesa_ci_internal.git">
      <remote name="internal-github" repo="git@github.com:intel-sandbox/mesa_ci_mesa_ci_internal.git"/>
    </mesa_ci_internal>

    <sim-drm repo="ssh://git@gitlab.devtools.intel.com:29418/mesa/sim-drm.git">
      <remote name="internal-github" repo="git@github.com:intel-innersource/drivers.gpu.mesa.sim-drm.git"/>
    </sim-drm>

    <!-- <skqp repo="https://skia.googlesource.com/skia" -->
    <!--   branch="2441c92442"> -->
    <!-- </skqp> -->

    <!-- The following are dependencies for SKQP -->
    <!-- <google_buildtools repo="https://chromium.googlesource.com/chromium/buildtools.git" /> -->
    <!-- <google_common repo="https://skia.googlesource.com/common.git" /> -->
    <!-- <angle2 repo="https://chromium.googlesource.com/angle/angle.git" /> -->
    <!-- <dng_sdk repo="https://android.googlesource.com/platform/external/dng_sdk.git" /> -->
    <!-- <expat repo="https://android.googlesource.com/platform/external/expat.git" /> -->
    <!-- <freetype repo="https://skia.googlesource.com/third_party/freetype2.git" /> -->
    <!-- <harfbuzz repo="https://skia.googlesource.com/third_party/harfbuzz.git" /> -->
    <!-- <icu repo="https://chromium.googlesource.com/chromium/deps/icu.git" /> -->
    <!-- <imgui repo="https://skia.googlesource.com/external/github.com/ocornut/imgui.git" /> -->
    <!-- <jsoncpp repo="https://chromium.googlesource.com/external/github.com/open-source-parsers/jsoncpp.git" /> -->
    <!-- <libjpeg-turbo repo="https://skia.googlesource.com/external/github.com/libjpeg-turbo/libjpeg-turbo.git" /> -->
    <!-- <libpng repo="https://skia.googlesource.com/third_party/libpng.git" /> -->
    <!-- <libwebp repo="https://chromium.googlesource.com/webm/libwebp.git" /> -->
    <!-- <lua repo="https://skia.googlesource.com/external/github.com/lua/lua.git" /> -->
    <!-- <microhttpd repo="https://android.googlesource.com/platform/external/libmicrohttpd" /> -->
    <!-- <piex repo="https://android.googlesource.com/platform/external/piex.git" /> -->
    <!-- <sdl repo="https://skia.googlesource.com/third_party/sdl" /> -->
    <!-- <sfntly repo="https://chromium.googlesource.com/external/github.com/googlei18n/sfntly.git" /> -->
    <!-- <skcms repo="https://skia.googlesource.com/skcms" /> -->
    <!-- <zlib repo="https://chromium.googlesource.com/chromium/src/third_party/zlib" /> -->
    <!-- <skqp_assets repo="https://gitlab.freedesktop.org/Mesa_CI/skqp_assets.git" /> -->

    <webgl repo="https://github.com/KhronosGroup/WebGL.git"/>
    <amber repo="https://github.com/google/amber.git"/>
  </repos>


  <branches>
    <!-- the following branches are polled continuously.  Any commit
         will trigger a branch build with an identifier based on the
         commit that triggered the build.  Any repository listed as a
         subtag of the branch can trigger a build of the branch.
         Repositories default to origin/master -->

    <!-- jenkins has a build with same name as branch -->
    <branch name="mesa_master" project="all-test" priority="2">

      <!-- these repo tags exist soley to trigger a master build when
           anything changes -->
      <mesa branch="origin/main"/>
      <piglit/>
      <waffle/>
      <drm/>
      <vkrunner/>
      <deqp branch="remotes/origin/upstream-vulkan-cts-1.2.7"/>
      <cts branch="origin/opengl-es-cts-3.2.8"/>
      <!-- skqp is pinned since newer versions provide completely different
           interfaces for running/results -->
      <skqp branch="2441c92442"/>
     <!-- <webgl/> -->
      <glcts branch="origin/opengl-cts-4.6.2"/>

      <!-- the trigger attribute is used specify a branch that should
           be used, but prevent builds from triggering if the branch
           changes.  In this case, the prerelease repo contains test
           status for unreleased platforms.  We want to use the
           latest, but we don't want to retest every time a config
           item changes. -->
      <prerelease trigger="false"/>
    </branch>


    <branch name="mesa_21.3" project="all-test-vulkan">
      <mesa branch="origin/21.3"/>
      <crucible branch="d03f3fd" />
      <cts branch="cc0a2fe479f9c91cd8577ce872950587d191a89c" />
      <deqp branch="dfbc398546de9162581869c4818603c3ca07a2b1" />
      <drm branch="dd3655ce" />
      <glcts branch="501679ad2d24cbfbd70c35ec459034a7cde41a82" />
      <piglit branch="83bc56abf2" />
      <vkrunner branch="1b4cc6b" />
      <vulkancts branch="39e596640" />
      <waffle branch="0a1fb6f" />
    </branch>

    <branch name="mesa_21.3_staging" project="all-test-vulkan">
      <mesa branch="origin/staging/21.3"/>
      <crucible branch="d03f3fd" />
      <cts branch="cc0a2fe479f9c91cd8577ce872950587d191a89c" />
      <deqp branch="dfbc398546de9162581869c4818603c3ca07a2b1" />
      <drm branch="dd3655ce" />
      <glcts branch="501679ad2d24cbfbd70c35ec459034a7cde41a82" />
      <piglit branch="83bc56abf2" />
      <vkrunner branch="1b4cc6b" />
      <vulkancts branch="39e596640" />
      <waffle branch="0a1fb6f" />
    </branch>

    <branch name="mesa_21.2" project="all-test-vulkan">
      <mesa branch="origin/21.2"/>
      <crucible branch="12749db" />
      <cts branch="cc0a2fe479f9c91cd8577ce872950587d191a89c" />
      <deqp branch="dfbc398546de9162581869c4818603c3ca07a2b1" />
      <drm branch="9cef5dee" />
      <glcts branch="501679ad2d24cbfbd70c35ec459034a7cde41a82" />
      <piglit branch="e535c2726c" />
      <vkrunner branch="1b4cc6b" />
      <vulkancts branch="27e691f02" />
      <waffle branch="6bf58d4" />
    </branch>

    <branch name="mesa_21.2_staging" project="all-test-vulkan">
      <mesa branch="origin/staging/21.2"/>
      <crucible branch="12749db" />
      <cts branch="cc0a2fe479f9c91cd8577ce872950587d191a89c" />
      <deqp branch="dfbc398546de9162581869c4818603c3ca07a2b1" />
      <drm branch="9cef5dee" />
      <glcts branch="501679ad2d24cbfbd70c35ec459034a7cde41a82" />
      <piglit branch="e535c2726c" />
      <vkrunner branch="1b4cc6b" />
      <vulkancts branch="27e691f02" />
      <waffle branch="6bf58d4" />
    </branch>

    <branch name="vulkancts" project="vulkancts-full" priority="2">
      <mesa branch="origin/main"/>
      <vulkancts/>
      <crucible/>
      <glslang/>
      <spirvheaders branch="801cca8104245c07e8cc53292da87ee1b76946fe"/>
      <spirvtools/>
    </branch>

    <branch name="mesa_master_crocus" project="test-single-arch-crocus" priority="2">
      <mesa branch="origin/main"/>
    </branch>

    <branch name="aswarup" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/aswarup"/>
    </branch>

    <branch name="jekstrand_gl" project="test-single-arch" priority="2">
      <mesa branch="jekstrand/jenkins_gl"/>
    </branch>

    <branch name="jekstrand_vulkan" project="test-vulkan" priority="2">
      <mesa branch="jekstrand/jenkins_vulkan"/>
    </branch>

    <branch name="jljusten_internal" project="test-single-arch" priority="2">
      <mesa branch="internal-gitlab/jljusten/jenkins/internal"/>
    </branch>

    <branch name="jljusten_tgl" project="test-single-arch-tgl" priority="2">
      <mesa branch="internal-gitlab/jljusten/jenkins/tgl"/>
    </branch>

    <branch name="kwg" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/kwg"/>
    </branch>

    <branch name="kwg_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/kwg_vulkan"/>
    </branch>

    <branch name="idr" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/idr/jenkins"/>
    </branch>

    <branch name="mattst88" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/mattst88"/>
    </branch>

    <branch name="dcbaker" project="test-single-arch-vulkan" priority="2">
      <piglit branch="ci_piglit_repo/dev/dcbaker"/>
    </branch>

    <branch name="jljusten" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/jljusten"/>
    </branch>

    <branch name="tpalli" project="test-single-arch-vulkan" priority="2">
      <mesa branch="tpalli/jenkins"/>
    </branch>

    <branch name="tpalli_vulkancts" project="test-vulkan" priority="2">
      <mesa branch="tpalli/jenkins-vkglcts"/>
    </branch>

    <branch name="curro" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/curro"/>
    </branch>

    <branch name="curro_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/curro-vk"/>
    </branch>

    <branch name="aphogat" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/aphogat"/>
    </branch>

    <branch name="aphogat_internal" project="test-single-arch-vulkan" priority="2">
      <mesa branch="internal-gitlab/aphogat/jenkins"/>
    </branch>

    <branch name="nchery_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/nchery_vulkan"/>
    </branch>

    <branch name="nchery" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/nchery"/>
    </branch>

    <branch name="nchery_tgl" project="test-single-arch-tgl" priority="2">
      <mesa branch="internal-gitlab/nchery/wip/tgl"/>
    </branch>

    <branch name="nchery_tgl-sim" project="test-single-arch-tgl-sim" priority="2">
      <mesa branch="internal-gitlab/nchery/wip/tgl-simdrm"/>
    </branch>

    <branch name="nchery_piglit" project="piglit-gen12" priority="2">
      <piglit branch="nchery-gitlab/dmabuf-files"/>
    </branch>

    <branch name="tarceri" project="test-single-arch-vulkan" priority="2">
      <mesa branch="tarceri/intel_ci"/>
    </branch>

    <branch name="evelikov" project="test-single-arch-vulkan" priority="2">
      <mesa branch="evelikov/intel-ci"/>
    </branch>

    <!-- These branches exists to separate build results from
         mesa_master on the server. -->
    <branch name="nir_torture_test" project="test-single-arch"/>
    <branch name="shader_cache_test" project="test-single-arch"/>
    <branch name="mesa_custom" project="all-test"/>
    <branch name="deqp-runtime_test" project="test-single-arch"/>

    <branch name="imirkin" project="test-single-arch-vulkan" priority="2">
      <mesa branch="imirkin/jenkins"/>
    </branch>

    <branch name="djdeath_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/djdeath_vulkan"/>
    </branch>

    <branch name="djdeath" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/djdeath"/>
    </branch>

    <branch name="jmcasanova" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/jmcasanova"/>
    </branch>

    <branch name="itoral" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/itoral"/>
    </branch>

    <branch name="agomez" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/agomez"/>
    </branch>

    <branch name="siglesias" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/siglesias"/>
    </branch>

    <branch name="apinheiro" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/apinheiro"/>
    </branch>

    <branch name="chadv" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/chadv/vulkan"/>
    </branch>

   <branch name="daniels" project="test-single-arch-vulkan" priority="2">
      <mesa branch="daniels/ci/intel"/>
    </branch>
    
    <branch name="cmarcelo_gl" project="test-single-arch" priority="2">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_gl"/>
    </branch>

    <branch name="cmarcelo_vulkan" project="test-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_vulkan"/>
    </branch>

    <branch name="cmarcelo_tgl" project="test-single-arch-tgl" priority="2">
      <mesa branch="internal-gitlab/cmarcelo/jenkins/tgl"/>
    </branch>

    <branch name="cmarcelo_iris" project="all-test-iris" priority="2">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_iris"/>
    </branch>

    <branch name="sagarghuge" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/sagarghuge"/>
    </branch>

    <branch name="sagarghuge-tgl" project="test-single-arch-tgl" priority="2">
      <mesa branch="internal-gitlab/sghuge/jenkins/tgl"/>
    </branch>

    <branch name="sagarghuge-tgl-sim" project="test-single-arch-tgl-sim" priority="2">
      <mesa branch="internal-gitlab/sghuge/jenkins/tgl-simdrm"/>
    </branch>

    <branch name="anholt" project="test-single-arch-anholt" priority="2">
      <mesa branch="anholt/jenkins"/>
    </branch>

    <branch name="eric_engestrom" project="test-single-arch-vulkan" priority="2">
      <mesa branch="eric_engestrom/jenkins"/>
    </branch>

    <branch name="frohlich" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/frohlich"/>
    </branch>

    <branch name="fjdegroo" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/fjdegroo"/>
    </branch>

    <branch name="airlied" project="test-single-arch-vulkan" priority="2">
      <mesa branch="airlied/jenkins"/>
    </branch>

    <branch name="global_logic" project="test-single-arch-vulkan" priority="2">
      <mesa branch="global_logic/i965_ci"/>
    </branch>

    <branch name="global_logic_2" project="test-single-arch-vulkan" priority="2">
      <mesa branch="global_logic/i965_second"/>
    </branch>

    <branch name="karolherbst" project="test-single-arch-vulkan" priority="2">
      <mesa branch="karolherbst/jenkins"/>
    </branch>

    <branch name="cwabbott" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/cwabbott"/>
    </branch>
    
    <branch name="kusma" project="test-single-arch-vulkan" priority="2">
      <mesa branch="kusma/intel-ci"/>
    </branch>
    
    <branch name="gerddie" project="test-single-arch-vulkan" priority="2">
      <mesa branch="gerddie/intel-ci"/>
    </branch>

    <branch name="agoldmints" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/igalia/agoldmints"/>
    </branch>

    <branch name="pzanoni" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/pzanoni"/>
    </branch>

    <branch name="pzanoni_internal" project="test-single-arch-vulkan" priority="2">
      <mesa branch="internal-gitlab/pzanoni/jenkins"/>
    </branch>

    <branch name="ibriano" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/Sachiel"/>
    </branch>

    <branch name="pendingchaos" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/pendingchaos"/>
    </branch>

    <branch name="otaviobp" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/otaviobp"/>
    </branch>

    <branch name="jzhang80" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/jzhang80"/>
    </branch>

    <!-- Jobs for this branch are triggered manually -->
    <branch name="internal" project="test-single-arch-internal" priority="2"/>

    <branch name="anholt" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/anholt"/>
    </branch>

    <branch name="mareko" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/mareko"/>
    </branch>

    <branch name="scottph" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/scottph"/>
    </branch>

    <branch name="shadeslayer" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/shadeslayer"/>
    </branch>

    <branch name="mslusarz" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/mslusarz"/>
    </branch>

    <branch name="jenatali" project="test-single-arch-vulkan" priority="2">
      <mesa branch="ci_mesa_repo/dev/jenatali"/>
    </branch>

    <branch name="internal_dg1" project="test-single-arch-dg1" priority="2">
      <mesa branch="internal-gitlab/jljusten/wip/tgl"/>
    </branch>

  </branches>

</build_specification>
